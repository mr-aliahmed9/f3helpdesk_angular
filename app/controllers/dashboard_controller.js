// Dashboard Controller
f3HelpDesk.controller("DashboardController", function($scope, cfpLoadingBar, $rootScope, localStorageService, humanizeFilter, Session, $uibModal, __config, $log, allTickets, allAssignedAgents, allAssignedTickets, allAssgneeCustomers, Ticket) {
	"use strict";
	$scope.sessionSession   = Session;
	$scope.ticketService    = Ticket;
  $scope.current_user     = $scope.sessionSession.auth();
  $scope.pdfGetURL        = __config.apiBaseUrl + __config.apiUrls.downloadPDFUrl;

	// As our current tab will be the High Priority Tab
	$scope.mainPanelTitle   = "High Priority";
	$rootScope.currentScope = "high_priority";

	$scope.text = "Welcome " + $scope.current_user.data.full_name;

	$scope.init = function () {
		// Remove stored filters to load default scope
		localStorageService.remove("scopeFilters");
	};

	$scope.openTicketForm = function (size) {
    var modalInstance = $uibModal.open({
			animation: true,
      templateUrl: 'newTicketModal.html',
			controller: 'TicketModalInstanceController',
			keyboard: false,
			backdrop: "static",
      size: size
    });
  };

	$scope.dtPopUp = {
    opened: false,
		format: 'yyyy/MM/dd',
		dateOptions: {
	    formatYear: 'yy',
	    maxDate: new Date(),
	    startingDay: 1
		},
		altInputFormats: ['M!/d!/yyyy']
  };

	$scope.openDatePicker = function() {
    $scope.dtPopUp.opened = true;
  };

	// Injected Data are now saved to scope variables
	$scope.tickets = allTickets;

	if(angular.isDefined(allAssignedAgents)) {
		$scope.customerAgents = allAssignedAgents.data.assigned_agents;
	}

	if(angular.isDefined(allAssgneeCustomers)) {
		$scope.customers = allAssgneeCustomers.data.customers;
	}

	// Function to get tickets by scope
	$scope.fetchTicketsByScope = function(scope, options) {
		try {
			if(angular.isUndefined(options)) {
				var options = {};
			}

			if(angular.isUndefined(options["query"])) {
				// Start the loader bar on every request
				cfpLoadingBar.start();
			}

			// Inserting params
			var $params = {
				scope: scope,
				query: options["query"]
			}

			// If user is agent and current scope is "in process" or "closed"
			// Then agent would want to see the assigned issues.
			if($scope.current_user.isAgent()) {
				if($params.scope == "in_process" || $params.scope == "closed") {
					$params.agent_tickets = true;
				}
			}

			// Added tags in the params
			if(angular.isDefined(options["tag_list"])) {
				$params["tag_list[]"] = [];
				 $.map(options["tag_list"], function(value, index) {
					console.log(value)
					$params["tag_list[]"].push(value.text);
				});
			}

			// Persisting the filters in the local storage
			// next request params will get merge with the new request params
			// e.g -> if user want to search with multiple filters
			var $scopeFilters = localStorageService.get("scopeFilters");
		  if($scopeFilters != null) {
				$.map($scopeFilters, function(scopeValue, index) {
				 $.map($params, function(paramValue, index) {
					 if(paramValue != undefined && scopeValue != paramValue) {
						 $scopeFilters[index] = $params[index];
					 }
				 });
			 });
		  } else {
				// If no local storage present then creating new one
				$scopeFilters = $params;
				localStorageService.set("scopeFilters", $params);
		  }

			// Calling API for fetching scoped based tickets of users
			Ticket.fetchTickets($scopeFilters)
			.then(function(response) {
				cfpLoadingBar.complete();        // Auto stopping loading after success call.
				$rootScope.currentScope = scope; // Setting new scope
				$scope.tickets = response;       // Adding tickets

				// Reseting Filters to cover on all scopes
				if(angular.isDefined($scopeFilters)) {
					localStorageService.set("scopeFilters", $scopeFilters);
				} else {
					localStorageService.set("scopeFilters", $params);
				}

				// If scope is other than agents assigned tickets related then
				// removing agent tickets param
				if($scope.current_user.isAgent()) {

					var $setFilters = localStorageService.get("scopeFilters");

					delete $setFilters.agent_tickets;
					localStorageService.set("scopeFilters", $setFilters);
				}

			})
			.catch(function(response) {
				cfpLoadingBar.complete();
				console.log("Server Error", response.error);
				$.alert("Something went wrong. Contact Tech Support", "500 - Internal Server Error");
				$scope.tickets = [];
		  })
		} catch(exception) {
			$.alert("The server is not responding at the moment.", "503 - Service Unavailable");
		}
	}

	// Watching current scope variable so
	// Updating the Panel Title
	$scope.$watch('currentScope', function(new_val, old_val) {
		$scope.mainPanelTitle = humanizeFilter(new_val)
  });

	// This event is being used for triggering out
	// when ticket modal is closed after success call
	$rootScope.$on('ticketFetchEvent', function(event, scopeObj) {
    $scope.fetchTicketsByScope(scopeObj.scope_name, scopeObj.options);
  });
});

// Ticket Form for customers
// Used through Bootstrap Modal Instance.
// It is used as a seperate controller instance
f3HelpDesk.controller("TicketModalInstanceController", function($http, $scope, $rootScope, Session, $uibModalInstance, __config, $log, Ticket, Helper) {
	"use strict";
	$scope.session = Session;
  $scope.current_user = $scope.session.auth();
	$scope.ticketService = Ticket;

	// All The priorities
	$scope.priorities = ['High', 'Medium', 'Low'];

	$scope.parameters = {};

	// Creating a new ticket service
	$scope.saveNewTicket = function(status) {
		if(angular.isUndefined(status)) {
			status = 0;
		} else {
			switch(status) {
				case "open":
					status = 1
					break;
				case "closed":
					status = 3
					break;
			}
		}

		var $tags = $.map($scope.parameters.tags, function(value, index) {
			return [value.text];
		});

		console.log($tags)

		var $formParams = {
			ticket: {
				title: $scope.parameters.title,
				description: $scope.parameters.description,
				priority: $scope.parameters.priority,
				status: status,
			}
		}

		$formParams.ticket.tag_list = $tags

		Ticket.createTicket($formParams)
			.then(function(response) {
				if(response.status === "error") {
					Helper.flash("danger", "Error", response.message);
				} else {
					$rootScope.$emit("ticketFetchEvent", {
						scope_name: $rootScope.currentScope
					});
					$scope.closeTicketForm();
					Helper.flash("success", "Instructions", response.message);
				}

			})
			.catch(function(response) {
				Helper.flash("danger", "error", response.error);
		  })
	}

	$scope.closeTicketForm = function () {
		$uibModalInstance.dismiss('cancel');
	}
});

// Tickets Controller
f3HelpDesk.controller("TicketsController", function($http, $scope, $rootScope, Session, __config, $log, Ticket, Helper) {
	"use strict";
	$scope.session = Session
  $scope.current_user = $scope.session.auth();
	$scope.ticketService = Ticket;


	$scope.updateTicketStatus = function(status, ticket_id) {
		var $params = {
			ticket: {
				id: ticket_id,
				status: status,
			}
		}
		Ticket.setStatus($params)
			.then(function(response) {
				if(response.status === "error") {
					Helper.flash("danger", "Error", response.message);
				} else {
					$rootScope.$emit("ticketFetchEvent", {
						scope_name: $rootScope.currentScope
					});
				}
			})
			.catch(function(response) {
				Helper.flash("danger", "error", response.error);
		  });
	}

	$scope.assignTicket = function(ticket_id, agent_id, customer_id) {
		var $params = {
			ticket_assignment: {
				ticket_id: ticket_id,
				customer_id: customer_id,
			},
			agent_id: agent_id
		}
		Ticket.createTicketAssignment($params)
			.then(function(response) {
				if(response.status === "error") {
					Helper.flash("danger", "Error", response.message);
				} else {
					$rootScope.$emit("ticketFetchEvent", {
						scope_name: "in_process",
						options: {
							assigned_to_me: true
						}
					});
					Helper.flash("info", "", response.message);
				}

			})
			.catch(function(response) {
				Helper.flash("danger", "error", response.error);
		  })
	}

	$scope.deleteAssignment = function(event, agent_id, customer_id, user_role) {
		$.confirm({
	    title: 'Confirm!',
	    content: 'Remove Assignment?',
	    confirm: function(){
				Ticket.removeTicketAssignment(agent_id, customer_id, user_role)
					.then(function(response) {
						$(event.target).closest("a").remove();
						$.alert(response.message)
					})
					.catch(function(response) {
						Helper.flash("danger", "error", response.error);
				  })
	    }
		});
	}

	$scope.deleteTicket = function(event, ticket_id) {
		$.confirm({
	    title: 'Confirm!',
	    content: 'Remove Ticket?',
	    confirm: function(){
				Ticket.removeTicket(ticket_id)
					.then(function(response) {
						$(event.target).closest("tr").remove();
						$rootScope.$emit("ticketFetchEvent", {
							scope_name: $rootScope.currentScope
						});
						$.alert(response.message)
					})
					.catch(function(response) {
						Helper.flash("danger", "error", response.error);
				  })
	    }
		});
	}
})
