// Be descriptive with titles here. The describe and it titles combined read like a sentence.
describe('Welcoming Customers', function() {
  beforeEach(module('f3HelpDesk'));

  var $controller;

  beforeEach(inject(function(_$controller_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $controller = _$controller_;
  }));

  describe('$scope.text', function() {
    it('welcomes customers', function() {
      var $scope = {};
      var controller = $controller('WelcomeController', { $scope: $scope });
      expect($scope.text).toEqual('Bonjour!!');
    });
  });
});
