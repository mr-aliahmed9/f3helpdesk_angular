f3HelpDesk.controller("RegistrationsController", function($scope, Session, Registration, Helper) {
	"use strict";
  this.parameters = {};

	this.signUp = function(){
    Registration.createUserAccount(this.parameters)
    .then(function(response) {
			Helper.flash("info", "success", response.message);
			Helper.redirect_to("/users/sign_in");
		})
		.catch(function(response) {
			// Registration failed...
			try {
				Helper.flash("danger", "error", response.error);
			} catch(exception) {
				$.alert("The server is not responding at the moment.", "503 - Service Unavailable");
			}
	  })
  }
})
