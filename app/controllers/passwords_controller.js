f3HelpDesk.controller("PasswordsController", function($scope, $rootScope, $location, Session, Registration, Helper, Password) {
	"use strict";
  $scope.parameters = {};
	$scope.is_new_user = $location.search().is_new_user;

	$scope.updatePassword = function() {
		var reset_token = $location.search().token;
		console.log($location.search().token)

		if($scope.is_new_user) {
			$scope.parameters.is_new_user = $scope.is_new_user;
		}

		console.log($scope.parameters)

		Password.saveUserPassword($scope.parameters, reset_token)
		.then(function(response) {
			$rootScope.hideNav = false;
			Helper.flash("info", "success", response.message);
			Helper.redirect_to("/users/sign_in");
		})
		.catch(function(response) {
			Helper.flash("danger", "error", response.error);
	  })
	}

	$scope.resetPassword = function() {
		Password.requestNewPassword($scope.parameters.email)
		.then(function(response) {
			$rootScope.hideNav = false;
			Helper.flash("info", "success", "Your request has been sent. If your email address exists then we will send you the password instructions soon.");
			Helper.redirect_to("/home");
		})
		.catch(function(response) {
			try {
				Helper.flash("danger", "error", response.error);
			} catch(exception) {
				$.alert("The server is not responding at the moment.", "503 - Service Unavailable");
			}
	  })
	}
})
