f3HelpDesk.controller("AuthenticationsController", function($rootScope, Session, Helper) {
	"use strict";
	var authController = this;
	// Session Service
  authController.session = Session;

  authController.rememberMe = function($event) {
    if(this.remember_me === "false") {
      $('.checkbox').addClass('show');
      this.remember_me = "true"
    } else {
      $('.checkbox').removeClass('show');
      this.remember_me = "false"
    }
    console.log(this.remember_me)
  }

  authController.loginUser = function(form) {
    var credentials = {
      email: form.email.$viewValue,
      password: form.password.$viewValue
    };
    Session.login(credentials)
		.then(function(response) {
			Helper.flash("info", "Instructions", "Logged in successfully");
			Helper.redirect_to("/dashboard");
		})
		.catch(function(response) {
			// Authentication failed...
			try {
				Helper.flash("danger", "error", response.error);
			} catch(exception) {
				$.alert("The server is not responding at the moment.", "503 - Service Unavailable");
			}
	  })
  }
})
