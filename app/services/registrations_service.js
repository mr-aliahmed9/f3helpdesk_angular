f3HelpDesk.factory('Registration', function($q, $rootScope, Upload, $location, __config, localStorageService, $http, Helper, $window) {

    var service = {
      userParams: function(params) {
        var data = {};
        data = {
          first_name: params.first_name ? params.first_name : "",
          last_name: params.last_name ? params.last_name : "",
          contact: params.contact ? params.contact : "",
          email: params.email ? params.email : "",
          password: params.password ? params.password : "",
          password_confirmation: params.password_confirmation ? params.password_confirmation : ""
        }

        return data;
      },
      sendPayload: function(formData, method, url) {
        var file_attachment, options, ref, ref1;
        avatar = (ref = formData.avatar) != null ? ref : [];
        console.log(avatar)
        options = {
          url: url,
          method: method,
          file: avatar,
          file_form_data_name: (ref1 = avatar.name) != null ? ref1 : "",
          fields: {
            user: service.userParams(formData)
          }
        };

        return options;
      },
      createUserAccount: function(parameters) {

        var url = __config.apiBaseUrl + __config.apiUrls.registrationUrl;
        var deferred = $q.defer();

        Upload.upload(service.sendPayload(parameters, "POST", url))
        .success(function(response) {
          // Returns user
          console.log("Account Created");
          deferred.resolve(response);
        }).error(deferred.reject);

        return deferred.promise;
      }
    };

    return service;
  });
