f3HelpDesk.factory('Helper', function($location, $http, $q, capitalizeFilter) {
  var helper_methods = {
    authenticated_user: function(url, method){
      // Request requires authorization
		  // Will cause a `401 Unauthorized` response,
		  // that will be recovered by listener in devise config JS.
		  $http({
			  method: method,
			  url: url
			}).then(function successCallback(response) {
		    // console.log(response)
		  }, function errorCallback(response) {
		  	// console.log(response)
		  });
    },
    redirect_to: function(url) {
	  	url = url || '/';
	    $location.url(url);
	  },
    userParams: {
      user: {}
    },
	  // Bootstrap Notify Plugin
	  flash: function(flash_type, heading, content) {
	  	var icon_type;
	  	switch(flash_type) {
	  		case "success":
	  			icon_type = "ok"
	  		case "danger":
	  			icon_type = "remove"
	  	}
	  	$.notify({
        icon: 'glyphicon glyphicon-' + icon_type,
        title: '<strong>' + capitalizeFilter(heading) + '</strong>',
        message: content
      }, {
        type: flash_type
      });
	  }
  }
  return helper_methods;
});
