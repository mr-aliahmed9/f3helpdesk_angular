f3HelpDesk.factory('Session', function($q, $rootScope, $cookieStore, $location, __config, localStorageService, $http, Helper, $window) {
    var service = {
      requestHeaders: {},
      login: function(credentials) {
        requestHeaders = {
          'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;'
        }

        var deferred = $q.defer();

        $http({
          method: 'POST',
          url: __config.apiBaseUrl + __config.apiUrls.loginUrl,
          data: $.param(credentials),
          headers: requestHeaders
        }).success(function(response) {
          // Returns user
          $cookieStore.put('authenticatedUser', response);
          $rootScope.authentication = response;

          console.log('$rootScope.authentication', $rootScope.authentication);
          deferred.resolve(response.auth_token);
        }).error(deferred.reject);

        return deferred.promise;
      },
      logout: function() {
        $cookieStore.remove('authenticatedUser');
        $rootScope.authentication = null;
        Helper.flash("info", "", "Logged out successfully");
    		Helper.redirect_to("/home");
      },
      authenticatedUser: function() {
        if(!service.isAuthenticated()) {
          Helper.redirect_to("/users/sign_in")
          Helper.flash("info", "instructions", "You need to sign in before continue.")
        }
      },
      checkAlreadySignedIn: function() {
        if(service.isAuthenticated()) {
          Helper.redirect_to("/home")
          Helper.flash("info", "instructions", "You are already sign in")
        }
      },
      // Is user logged in
      isAuthenticated: function(){
        var user = $cookieStore.get('authenticatedUser');
        if(user != null && angular.isDefined(user)) {
          return true;
        } else {
          return false;
        }
      },
      sendAuthenticationRequest: function() {
        console.log(service.auth().auth_token)
        var deferred = $q.defer();
        $http({
          method: 'GET',
          url: __config.apiBaseUrl + __config.apiUrls.authenticationRequestUrl,
          headers: service.auth().auth_headers()
        }).success(function(response) {
          if(response.status === 401) {
            deferred.resolve(401);
          } else {
            deferred.resolve(response.status);
          }
        }).error(function(){
          $.alert("The server is not responding at the moment.", "503 - Service Unavailable");
        });

        return deferred.promise;
      },
      // Signed in user details
      auth: function() {
        var authentication = $cookieStore.get('authenticatedUser');
        var currentUser = {};
        if(angular.isDefined(authentication)) {
          currentUser = {
            data: authentication.user,
            auth_token: authentication.auth_token,
            isCustomer: function() {
        			if(authentication.user.role == "Customer") {
        				return true;
        			} else {
        				return false;
        			}
        		},
        		isAgent: function() {
        			if(authentication.user.role == "Agent") {
        				return true;
        			} else {
        				return false;
        			}
        		},
            auth_headers: function() {
              return {'Authorization': authentication.auth_token}
            }
          }
        } else {
          return null;
        }

        return currentUser;
      }

    };

    return service;
  });
