f3HelpDesk.factory('Ticket', function($q, $rootScope, $location, __config, localStorageService, $http, Helper, $window, Session) {

  var service = {
    createTicket: function(parameters) {
      var deferred = $q.defer();
      console.log(parameters)
      $http({
        method: 'POST',
        url: __config.apiBaseUrl + __config.apiUrls.newTicketUrl,
        data: parameters,
        headers: Session.auth().auth_headers()
      }).success(function(response) {
        deferred.resolve(response);
      }).error(deferred.reject);

      return deferred.promise;
    },
    fetchTickets: function(params) {
      var deferred = $q.defer();
      console.log("Ticket Auth Token", Session.auth().auth_headers())
      $http.get(__config.apiBaseUrl + __config.apiUrls.dashboardTicketsUrl, {
          params: params,
          headers: Session.auth().auth_headers()
       }).success(function (response) {
          deferred.resolve(response);
       }).error(deferred.reject);

       return deferred.promise;
    },
    loadTags: function($query) {
      var deferred = $q.defer();
  		$http.get(__config.apiBaseUrl + __config.apiUrls.tagsUrl, {
  			headers: Session.auth().auth_headers()
  		}).success(function (response) {
         deferred.resolve(response.data);
      });

      return deferred.promise;
  	},
    fetchAssignedAgents: function() {
  		return $http.get(__config.apiBaseUrl + __config.apiUrls.assignedAgentsUrl, {
  			headers: Session.auth().auth_headers()
  		});
  	},
    fetchAssignedTickets: function() {
  		return $http.get(__config.apiBaseUrl + __config.apiUrls.assignedTicketsUrl, {
  			headers: Session.auth().auth_headers()
  		});
  	},
    fetchAssgneeCustomers: function() {
  		return $http.get(__config.apiBaseUrl + __config.apiUrls.agentCustomersUrl, {
  			headers: Session.auth().auth_headers()
  		});
  	},
    setStatus: function(parameters) {
      var deferred = $q.defer();

      $http({
        method: 'PUT',
        url: __config.apiBaseUrl + __config.apiUrls.updateTicketUrl + parameters.ticket.id,
        data: parameters,
        headers: Session.auth().auth_headers()
      }).success(function(response) {
        deferred.resolve(response);
      }).error(deferred.reject);

      return deferred.promise;
    },
    createTicketAssignment: function(parameters) {
      var deferred = $q.defer();

      $http({
        method: 'POST',
        url: __config.apiBaseUrl + __config.apiUrls.assignTicketUrl,
        data: parameters,
        headers: Session.auth().auth_headers()
      }).success(function(response) {
        deferred.resolve(response);
      }).error(deferred.reject);

      return deferred.promise;
    },
    removeTicketAssignment: function(agent_id, customer_id, user_role) {
      var deferred = $q.defer();

      $http({
        method: 'DELETE',
        url: __config.apiBaseUrl + __config.apiUrls.removeAssignedTicketUrl + agent_id + "/" + customer_id + "/" + user_role,
        headers: Session.auth().auth_headers()
      }).success(function(response) {
        deferred.resolve(response);
      }).error(deferred.reject);

      return deferred.promise;
    },
    removeTicket: function(ticket_id) {
      var deferred = $q.defer();

      $http({
        method: 'DELETE',
        url: __config.apiBaseUrl + __config.apiUrls.updateTicketUrl + ticket_id,
        headers: Session.auth().auth_headers()
      }).success(function(response) {
        deferred.resolve(response);
      }).error(deferred.reject);

      return deferred.promise;
    }
  };

  return service;
});
