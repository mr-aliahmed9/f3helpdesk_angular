f3HelpDesk.factory('Password', function($q, $rootScope, Upload, $location, __config, localStorageService, $http, Helper, $window) {

    var service = {
      userParams: function(params) {
        var data = {};

        if(params.is_new_user) {
          data = {
            first_name: params.first_name ? params.first_name : "",
            last_name: params.last_name ? params.last_name : "",
            contact: params.contact ? params.contact : ""
          }
        }

        data.password = params.password ? params.password : "";
        data.password_confirmation = params.password_confirmation ? params.password_confirmation : "";

        return data;
      },
      sendPayload: function(formData, method, url) {
        var file_attachment, options, ref, ref1;
        avatar = (ref = formData.avatar) != null ? ref : [];

        options = {
          url: url,
          method: method,
          file: avatar,
          file_form_data_name: (ref1 = avatar.name) != null ? ref1 : "",
          fields: {
            user: service.userParams(formData)
          }
        };

        return options;
      },
      validatePasswordToken: function(token) {
        requestHeaders = {
          'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;'
        }

        var deferred = $q.defer();

        $http.get(__config.apiBaseUrl + __config.apiUrls.pwdTokenValidationUrl, {
          params: { token: token },
          headers: requestHeaders
        }).success(function(response) {
          deferred.resolve(response);
        }).error(deferred.reject);

        return deferred.promise;
      },
      saveUserPassword: function(parameters, token) {
        var deferred = $q.defer();

        console.log(parameters)
        var url = __config.apiBaseUrl + __config.apiUrls.updatePasswordUrl + token;
        var deferred = $q.defer();

        Upload.upload(service.sendPayload(parameters, "PUT", url))
        .success(function(response) {
          deferred.resolve(response);
        }).error(deferred.reject);

        return deferred.promise;
      },
      requestNewPassword: function(email) {
        requestHeaders = {
          'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;'
        }

        var deferred = $q.defer();
        $http({
          method: 'POST',
          url: __config.apiBaseUrl + __config.apiUrls.newPasswordUrl,
          data: $.param({email: email}),
          headers: requestHeaders
        }).success(function(response) {
          deferred.resolve(response);
        }).error(deferred.reject);

        return deferred.promise;
      }
    };

    return service;
  });
