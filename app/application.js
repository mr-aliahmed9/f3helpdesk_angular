f3HelpDesk = angular.module('f3HelpDesk', ['ngRoute', 'config', 'angular-loading-bar', 'ngAnimate', 'cfp.loadingBar', 'LocalStorageModule', 'ui.bootstrap', 'ngTagsInput', 'ngCookies', 'ngFileUpload']);

f3HelpDesk.run(function($rootScope, $location, $cookieStore, Session, __config, ENV) {
  // Setting API Base Url
  __config.apiBaseUrl = ENV.baseUrl;
  
  $rootScope.pageTitle = "f3 Help Desk";
  $rootScope.hideNav = false;
  $rootScope.location = $location;

  if(angular.isDefined($cookieStore.get('authenticatedUser'))) {
    $rootScope.authentication = $cookieStore.get('authenticatedUser')
    console.log("User", $rootScope.authentication)

    Session.sendAuthenticationRequest()
    .then(function(status) {
      if(status === 401) {
        $cookieStore.remove('authenticatedUser');
        $rootScope.authentication = null;
        Session.authenticatedUser();
      }
    });
  }
});

f3HelpDesk.config(function (localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix("f3 Help Desk");
});

f3HelpDesk.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
  cfpLoadingBarProvider.includeSpinner = true;
  cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
}]);
