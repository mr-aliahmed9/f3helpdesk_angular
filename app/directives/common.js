f3HelpDesk.directive('dashboardSideBar', function() {
  return {
    templateUrl: 'app/templates/partials/sidebar.html'
  };
});

f3HelpDesk.directive('ticketsTable', function() {
    return {
      restrict: 'A',
      replace: false,
      templateUrl: "app/templates/partials/tickets.html",
      scope: {
          tickets: "=tickets",
          currentUser: "=user",
          currentScope: "=currscope",
      }
    };
});


f3HelpDesk.directive("fileDownload", [
  "$window", function($window) {
    return {
      restrict: "A",
      scope: {
        record: "="
      },
      link: function($scope, element) {
        return element.on("click", function() {
          return $scope.record.customGET("file_attachment_url").then(function(response) {
            return $window.open(response);
          });
        });
      }
    };
  }
]);


f3HelpDesk.directive('ticketStatus', function() {
  return {
   restrict: 'A',
   replace: true,
   template: "",
   scope: {
     status: "=status"
   },
   link: function(scope, element){
     var html = "";
     switch(scope.status) {
       case "newly":
         html += '<span class="label label-default">New</span>';
         break;
       case "open":
         html += '<span class="label label-info">Open</span>';
         break;
       case "in_process":
         html += '<span class="label label-warning">In Process</span>';
         break;
       case "closed":
         html += '<span class="label label-danger">Closed</span>';
         break;
     }

      element.replaceWith(html);
   }
 }
});
