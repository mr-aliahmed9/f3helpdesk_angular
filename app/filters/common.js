f3HelpDesk.filter('capitalize', function() {
  return function(input) {
    return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
  }
});

f3HelpDesk.filter('humanize', function(capitalizeFilter) {
  return function(input) {
    value = input.split("_");
    value[0] = capitalizeFilter(value[0]);
    return value.join(" ")
  }
});

f3HelpDesk.filter('trusted', ['$sce', function ($sce) {
  return function(url) {
    return $sce.trustAsResourceUrl(url);
  };
}]);
