f3HelpDesk.config(function($routeProvider, __config, $locationProvider, $httpProvider) {
  $routeProvider.when("/", {
    templateUrl: __config.templateUrl+ "/welcome.html",
    controller: "WelcomeController",
    resolve: {
      load: function($q, $rootScope, $location, Session, Password, Helper) {}
    }
  }).when("/users/setup_password", {
    templateUrl: __config.templateUrl+"/users/setup_password.html",
    resolve: {
      load: function($q, $rootScope, $location, Session, Password, Helper) {
        $rootScope.hideNav = true;
        token = $location.search().token;

        if(angular.isUndefined(token)) {
          $rootScope.hideNav = false;
          Helper.flash("danger", "Error", "Bad request");
          Helper.redirect_to("/home");
          return
        }

        Password.validatePasswordToken(token)
        .catch(function(response) {
          $rootScope.hideNav = false;
    			try {
            if(response.status != 200) {
              Helper.flash("danger", "Error", "Bad request");
            }
    			} catch(exception) {
    				$.alert("The server is not responding at the moment.", "503 - Service Unavailable");
    			}
          Helper.redirect_to("/home");
    	  })
      }
    }
  }).when("/users/reset_password", {
    templateUrl: __config.templateUrl+"/users/reset_password.html"
  }).when("/users/sign_in", {
    templateUrl: __config.templateUrl+"/users/sign_in.html",
    controller: "AuthenticationsController",
    controllerAs: "auth",
    resolve: {
      load: function($location, Session) {
        Session.checkAlreadySignedIn();
      }
    }
  }).when("/dashboard", {
    templateUrl: __config.templateUrl+"/dashboard.html",
    controller: "DashboardController",
    controllerAs: "dashboard",
    resolve: {
      load: function($q, $rootScope, $location, Session, Helper) {
        Session.authenticatedUser()
      },
      allTickets: function($q, $rootScope, $location, Session, Helper, Ticket) {
        return Ticket.fetchTickets({scope: "high_priority"});
      },
      allAssignedAgents: function($q, $rootScope, $location, Session, Helper, Ticket) {
        if(Session.auth().isCustomer()) {
          return Ticket.fetchAssignedAgents();
        }
      },
      allAssignedTickets: function($q, $rootScope, $location, Session, Helper, Ticket) {
        if(Session.auth().isAgent()) {
          return Ticket.fetchAssignedTickets();
        }
      },
      allAssgneeCustomers: function($q, $rootScope, $location, Session, Helper, Ticket) {
        if(Session.auth().isAgent()) {
          return Ticket.fetchAssgneeCustomers();
        }
      }
    }
  }).otherwise({
    redirectTo: "/"
  });
})
.run( function($rootScope, $location, Session, Helper, localStorageService, Password, cfpLoadingBar) {
    // listener to watch route changes
    $rootScope.$on( "$routeChangeStart", function(event, next, current, localStorageService) {
      cfpLoadingBar.start();

      if (next.templateUrl == "/app/templates/welcome.html" ) {
        if(Session.isAuthenticated()) {
          cfpLoadingBar.complete();
      	  Helper.redirect_to("/dashboard");
      		return;
      	}
      }
    });

    // listener to watch route changes finished
    $rootScope.$on('$routeChangeSuccess', function () {
      cfpLoadingBar.complete();
    });
});
