var env = {};

// Importing env if present (from env.js)
if(window){
  Object.assign(env, window.__config);
}

f3HelpDesk.constant('__config', env);
