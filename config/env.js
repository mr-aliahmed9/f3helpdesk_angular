// Environment JS
// Contains environment variables
(function (window) {
  window.__config = window.__config || {};
  // Base url
  window.__config.baseUrl = '/';

  // Template url
  window.__config.templateUrl = '/app/templates';                         // Template Folders

  window.__config.apiUrls = {
    loginUrl: "/api/sessions",                                         // Authentication
    authenticationRequestUrl: "/api/send_authenticated_user",          // Get Authentication Response
    registrationUrl: "/api/registrations",                             // Registrations
    pwdTokenValidationUrl: "/api/passwords/validates_password_token",  // Validating Pwd token
    updatePasswordUrl: "/api/passwords/",                              // Reset Password, @params -> :token param
    newPasswordUrl: "/api/passwords/",                                 // Request Password
    newTicketUrl: "/api/tickets",                                      // New Ticket
    updateTicketUrl: "/api/tickets/",                                  // Update Ticket, @params -> :ticket_id
    assignTicketUrl: "/api/ticket_assignment",                         // Assign Ticket, @params -> form params
    removeAssignedTicketUrl: "/api/ticket_assignment/",                // Remove Assignment, @params -> :agent_id, :customer_id, :user_role
    tagsUrl: "/api/tags",                                              // Ticket Tags
    assignedAgentsUrl: "/api/assigned_agents/customer",                // Signed in customer agents
    assignedTicketsUrl: "/api/assigned_tickets/agent",                 // Signed in agent assigned tickets
    agentCustomersUrl: "/api/my_customers/agent",                      // Signed in agent customers
    dashboardTicketsUrl: "/api/dashboard",                             // Fetch tickets, @params -> filter params
    downloadPDFUrl: "/api/assignment_report.pdf",                      // Pdf Download
  }

  // Whether or not to enable debug mode
  // Setting this to false will disable console output
  window.__config.enableDebug = true;
}(this));
