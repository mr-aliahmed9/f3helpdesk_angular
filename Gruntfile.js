module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-ng-constant');

  grunt.initConfig({
    ngconstant: {
      // Options for all targets
      options: {
        space: '  ',
        wrap: '"use strict";\n\n {\%= __ngModule %}',
        name: 'config',
      },
      development: {
        options: {
          dest: 'app/config/initializers.js'
        },
        constants: {
          ENV: {
            name: 'development',
            baseUrl: 'http://localhost:3000',
          }
        }
      },
      production: {
        options: {
          dest: 'app/config/initializers.js'
        },
        constants: {
          ENV: {
            name: 'production',
            baseUrl: 'https://f3helpdesk-backend.herokuapp.com',
          }
        }
      },
    },
    sass: {
      dist: {
        files: {
          'assets/stylesheets/application.css': 'assets/stylesheets/sass/application.scss'
        }
      },
      options: {
        includePaths: [
          './libs/css-calc-mixin'
        ]
      }
    },
    coffee: {
      compile: {
        expand: true,
        flatten: true,
        files: {
          'assets/javascripts/application.js': 'assets/javascripts/coffeescripts/*.coffee', // 1:1 compile
          // 'path/to/another.js': ['path/to/sources/*.coffee', 'path/to/more/*.coffee'] // compile and concat into single file
        }
      }
    },
    watch: {
      source: {
        files: ['assets/stylesheets/sass/*.scss', 'assets/javascripts/coffeescripts/*.coffee'],
        tasks: ['sass:dist', 'coffee:compile'],
        options: {
          livereload: true, // needed to run LiveReload
        }
      }
    }
  });

  grunt.registerTask('serve', function (target) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'ngconstant:development', // ADD THIS
      'sass',
      'coffee',
      'watch'
    ]);
  });

  grunt.registerTask('build', [
    'ngconstant:production',
  ]);
};
