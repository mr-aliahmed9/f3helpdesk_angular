(function() {
  $.notifyDefaults({
    allow_dismiss: true,
    z_index: 10000,
    offset: {
      y: 50,
      x: 33
    },
    placement: {
      from: 'top',
      align: 'right'
    }
  });

}).call(this);
